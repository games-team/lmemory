#include <gtk/gtk.h>
#include <sys/types.h>
#include <sys/dir.h>
#include <ctype.h>
#include <time.h>
#include <stdlib.h>
#include "lmemory.h"


/* local prototypes */
int file_select (struct direct *entry);

GSList *
lmem_dir_list (GSList * file_list, gchar * directory_name)
{
  int count, i, j, offset, xpm_count, card;
  struct direct **files;
  char *filename;
  char *name;


  if (directory_name == NULL)
    {
      g_print ("Error getting path\n");
      return NULL;

    }

  count = scandir (directory_name, &files, file_select, NULL);

  card = NUM/match_card;
  if(same_card == 1) card = NUM;
  if (count < card)
    {
      g_print ("Not enough files in this directory\n");
      return NULL;
    }

  offset = lln_get_random((double) count);
  xpm_count = 0;
  for (i = 0; i < count; i++)
    {
	j = i + offset;
	if ( j >= count ) j = j - count;
	filename = g_strconcat (directory_name, g_strdup(files[j]->d_name), NULL);
	name = filename + strlen(filename) - 4;
	if ( !strcmp ( name, ".xpm" )) {
	  file_list = g_slist_append (file_list, filename);
	  xpm_count++;
	  if ( xpm_count == card ) return (file_list);
	}
    }
    g_print ("Not enough xpm files in this directory\n");
    return NULL;

}

int
file_select (struct direct *entry)
{				/* ignore . and .. entries */
  if ((strcmp (entry->d_name, ".") == 0) ||
      (strcmp (entry->d_name, "..") == 0))
    return (FALSE);
  else
    return (TRUE);
}
