#include<gtk/gtk.h>
#include<strings.h>
#include "lmem_menu.h"
/* This is the GtkItemFactoryEntry structure used to generate new menus.
   Item 1: The menu path. The letter after the underscore indicates an
           accelerator key once the menu is open.
   Item 2: The accelerator key for the entry
   Item 3: The callback function.
   Item 4: The callback action.  This changes the parameters with
           which the function is called.  The default is 0.
   Item 5: The item type, used to define what kind of an item it is.
           Here are the possible values:
           NULL               -> "<Item>"
           ""                 -> "<Item>"
           "<Title>"          -> create a title item
           "<Item>"           -> create a simple item
           "<CheckItem>"      -> create a check item
           "<ToggleItem>"     -> create a toggle item
           "<RadioItem>"      -> create a radio item
           <path>             -> path of a radio item to link against
           "<Separator>"      -> create a separator
           "<Branch>"         -> create an item to hold sub items (optional)
           "<LastBranch>"     -> create a right justified branch
*/

static GtkItemFactoryEntry menu_items[] = {
  { "/_Game",         NULL,         NULL, 0, "<Branch>" },
  { "/Game/_New",     "<control>N", lmem_Newgame, 1, NULL },
  { "/Game/sep1",     NULL,         NULL, 0, "<Separator>" },
  { "/Game/Quit",     "<control>Q", gtk_main_quit, 0, NULL },
  { "/_Level",      NULL,         NULL, 0, "<Branch>" },
  { "/Level/Little One",  NULL,  lmem_Level, 2, NULL },
  { "/Level/Beginner",  NULL,  lmem_Level, 3, NULL },
  { "/Level/Skilled",  NULL,  lmem_Level, 4, NULL },
  { "/Level/Master",  NULL,  lmem_Level, 5, NULL },
  { "/Level/Daemon",  NULL,  lmem_Level, 6, NULL },
  { "/_Options",      NULL,         NULL, 0, "<Branch>" },
  { "/Options/Match 3 Cards",  NULL,  lmem_Set_Option, 7, "<ToggleItem>" },
  { "/Options/Different Cards",  NULL,  lmem_Set_Option, 8, "<ToggleItem>" },
  { "/_Help",         NULL,         NULL, 0, "<LastBranch>" },
  { "/Help/How to play",  NULL,  lmem_rules, 0, NULL },
  { "/Help/Options",  NULL,  lmem_option, 0, NULL },
  { "/Help/sep1",     NULL,         NULL, 0, "<Separator>" },
  { "/Help/About",   NULL,         lmem_about, 0, NULL },
};

void get_main_menu( GtkWidget  *window,
                    GtkWidget **menubar )
{
  GtkItemFactory *item_factory;
  GtkAccelGroup *accel_group;
  gint nmenu_items = sizeof (menu_items) / sizeof (menu_items[0]);

  accel_group = gtk_accel_group_new ();

  /* This function initializes the item factory.
     Param 1: The type of menu - can be GTK_TYPE_MENU_BAR, GTK_TYPE_MENU,
              or GTK_TYPE_OPTION_MENU.
     Param 2: The path of the menu.
     Param 3: A pointer to a gtk_accel_group.  The item factory sets up
              the accelerator table while generating menus.
  */

  item_factory = gtk_item_factory_new (GTK_TYPE_MENU_BAR, "<main>",
                                       accel_group);

  /* This function generates the menu items. Pass the item factory,
     the number of items in the array, the array itself, and any
     callback data for the the menu items. */
  gtk_item_factory_create_items (item_factory, nmenu_items, menu_items, window);

  /* Attach the new accelerator group to the window. */
  gtk_window_add_accel_group (GTK_WINDOW (window), accel_group);

  if (menubar)
    /* Finally, return the actual menu bar created by the item factory. */
    *menubar = gtk_item_factory_get_widget (item_factory, "<main>");
}
