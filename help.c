/*
 * callbacks.c - UI interfaces for gxfrm
 * 2001-03-07 E. Brombaugh created
 * 2002-09-22 E. Brombaugh converted to Gtk only w/o gettext
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <gtk/gtk.h>
#include <stdio.h>
#include <string.h>

#include "lmem_menu.h"
#include "help.h"
#include "support.h"

GtkWidget *window_about;
GtkWidget *window_rules;
GtkWidget *window_option;

void
on_about_ok_clicked   (GtkButton   *button, gpointer  user_data)
{
  gtk_widget_destroy (window_about);
  window_about = NULL;
}

void
lmem_about ()
{
  GtkWidget *label;

  if(window_about == NULL)
  {
    /* create the window */
    window_about = create_window_about ();
    
    /* Set the about box labels */
    label = lookup_widget (window_about, "label_about_version");
    gtk_label_set_text(GTK_LABEL(label), VERSION);
    label = lookup_widget (window_about, "label_about_date");
    gtk_label_set_text(GTK_LABEL(label), __DATE__);
    
    /* display the window */
    gtk_widget_show(window_about);
  }
}

GtkWidget*
create_window_about (void)
{
  GtkWidget *window1;
  GtkWidget *vbox2;
  GtkWidget *label_about;
  GtkWidget *label_about_version;
  GtkWidget *label_about_date;
  GtkWidget *hbuttonbox1;
  GtkWidget *button1;

  window1 = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  gtk_object_set_data (GTK_OBJECT (window1), "window_about", window1);
  gtk_window_set_title (GTK_WINDOW (window1), "About LMemory");

  vbox2 = gtk_vbox_new (FALSE, 0);
  gtk_widget_ref (vbox2);
  gtk_object_set_data_full (GTK_OBJECT (window1), "vbox2", vbox2,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (vbox2);
  gtk_container_add (GTK_CONTAINER (window1), vbox2);

  label_about = gtk_label_new ("LMemory\nMemory Card Game\nby Xiaoguang Zhang");
  gtk_widget_ref (label_about);
  gtk_object_set_data_full (GTK_OBJECT (window1), "label_about", label_about,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (label_about);
  gtk_box_pack_start (GTK_BOX (vbox2), label_about, FALSE, FALSE, 5);
  gtk_misc_set_padding (GTK_MISC (label_about), 11, 0);

  label_about_version = gtk_label_new ("version");
  gtk_widget_ref (label_about_version);
  gtk_object_set_data_full (GTK_OBJECT (window1), "label_about_version", label_about_version,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (label_about_version);
  gtk_box_pack_start (GTK_BOX (vbox2), label_about_version, FALSE, FALSE, 0);

  label_about_date = gtk_label_new ("date");
  gtk_widget_ref (label_about_date);
  gtk_object_set_data_full (GTK_OBJECT (window1), "label_about_date", label_about_date,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (label_about_date);
  gtk_box_pack_start (GTK_BOX (vbox2), label_about_date, FALSE, FALSE, 0);

  hbuttonbox1 = gtk_hbutton_box_new ();
  gtk_widget_ref (hbuttonbox1);
  gtk_object_set_data_full (GTK_OBJECT (window1), "hbuttonbox1", hbuttonbox1,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (hbuttonbox1);
  gtk_box_pack_start (GTK_BOX (vbox2), hbuttonbox1, TRUE, TRUE, 0);

  button1 = gtk_button_new_with_label ("OK");
  gtk_widget_ref (button1);
  gtk_object_set_data_full (GTK_OBJECT (window1), "button1", button1,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (button1);
  gtk_container_add (GTK_CONTAINER (hbuttonbox1), button1);
  GTK_WIDGET_SET_FLAGS (button1, GTK_CAN_DEFAULT);

  gtk_signal_connect (GTK_OBJECT (button1), "clicked",
                      GTK_SIGNAL_FUNC (on_about_ok_clicked),
                      NULL);

  return window1;
}

void
on_rules_ok_clicked   (GtkButton   *button, gpointer  user_data)
{
  gtk_widget_destroy (window_rules);
  window_rules = NULL;
}


void
lmem_rules ()
{
  GtkWidget *label;

  if(window_rules == NULL)
  {
    /* create the window */
    window_rules = create_window_rules ();
    
    /* display the window */
    gtk_widget_show(window_rules);
  }
}

GtkWidget*
create_window_rules (void)
{
  GtkWidget *window1;
  GtkWidget *vbox2;
  GtkWidget *label_rules;
  GtkWidget *hbuttonbox1;
  GtkWidget *button1;

  GtkWidget *scrolled_window;

  window1 = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  gtk_object_set_data (GTK_OBJECT (window1), "window_rules", window1);
  gtk_window_set_title (GTK_WINDOW (window1), "LMemory Rules");

  vbox2 = gtk_vbox_new (FALSE, 0);
  gtk_widget_ref (vbox2);
  gtk_object_set_data_full (GTK_OBJECT (window1), "vbox2", vbox2,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (vbox2);
  gtk_container_add (GTK_CONTAINER (window1), vbox2);

  label_rules = gtk_label_new (rule_buf);
  gtk_widget_ref (label_rules);
  gtk_object_set_data_full (GTK_OBJECT (window1), "label_rules", label_rules,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (label_rules);
  gtk_box_pack_start (GTK_BOX (vbox2), label_rules, FALSE, FALSE, 5);
  gtk_misc_set_padding (GTK_MISC (label_rules), 11, 0);

  hbuttonbox1 = gtk_hbutton_box_new ();
  gtk_widget_ref (hbuttonbox1);
  gtk_object_set_data_full (GTK_OBJECT (window1), "hbuttonbox1", hbuttonbox1,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (hbuttonbox1);
  gtk_box_pack_start (GTK_BOX (vbox2), hbuttonbox1, TRUE, TRUE, 0);

  button1 = gtk_button_new_with_label ("OK");
  gtk_widget_ref (button1);
  gtk_object_set_data_full (GTK_OBJECT (window1), "button1", button1,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (button1);
  gtk_container_add (GTK_CONTAINER (hbuttonbox1), button1);
  GTK_WIDGET_SET_FLAGS (button1, GTK_CAN_DEFAULT);

  gtk_signal_connect (GTK_OBJECT (button1), "clicked",
                      GTK_SIGNAL_FUNC (on_rules_ok_clicked),
                      NULL);

  return window1;
}

void
on_option_ok_clicked   (GtkButton   *button, gpointer  user_data)
{
  gtk_widget_destroy (window_option);
  window_option = NULL;
}


void
lmem_option ()
{
  GtkWidget *label;

  if(window_option == NULL)
  {
    /* create the window */
    window_option = create_window_option ();
    
    /* display the window */
    gtk_widget_show(window_option);
  }
}

GtkWidget*
create_window_option (void)
{
  GtkWidget *window1;
  GtkWidget *vbox2;
  GtkWidget *label_option;
  GtkWidget *hbuttonbox1;
  GtkWidget *button1;

  GtkWidget *scrolled_window;

  window1 = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  gtk_object_set_data (GTK_OBJECT (window1), "window_option", window1);
  gtk_window_set_title (GTK_WINDOW (window1), "LMemory option");

  vbox2 = gtk_vbox_new (FALSE, 0);
  gtk_widget_ref (vbox2);
  gtk_object_set_data_full (GTK_OBJECT (window1), "vbox2", vbox2,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (vbox2);
  gtk_container_add (GTK_CONTAINER (window1), vbox2);

  label_option = gtk_label_new (option_buf);
  gtk_widget_ref (label_option);
  gtk_object_set_data_full (GTK_OBJECT (window1), "label_option", label_option,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (label_option);
  gtk_box_pack_start (GTK_BOX (vbox2), label_option, FALSE, FALSE, 5);
  gtk_misc_set_padding (GTK_MISC (label_option), 11, 0);

  hbuttonbox1 = gtk_hbutton_box_new ();
  gtk_widget_ref (hbuttonbox1);
  gtk_object_set_data_full (GTK_OBJECT (window1), "hbuttonbox1", hbuttonbox1,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (hbuttonbox1);
  gtk_box_pack_start (GTK_BOX (vbox2), hbuttonbox1, TRUE, TRUE, 0);

  button1 = gtk_button_new_with_label ("OK");
  gtk_widget_ref (button1);
  gtk_object_set_data_full (GTK_OBJECT (window1), "button1", button1,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (button1);
  gtk_container_add (GTK_CONTAINER (hbuttonbox1), button1);
  GTK_WIDGET_SET_FLAGS (button1, GTK_CAN_DEFAULT);

  gtk_signal_connect (GTK_OBJECT (button1), "clicked",
                      GTK_SIGNAL_FUNC (on_option_ok_clicked),
                      NULL);

  return window1;
}
